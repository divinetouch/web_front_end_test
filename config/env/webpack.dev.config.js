let webpack = require('webpack')

let env_variables = {
  'process.env': {
    DEBUG: JSON.stringify(true),
    API_URL: JSON.stringify('http://localhost:8002'),
  }
}

module.exports = function (ROOT) {
  return {
    mode: 'development',
    devtool: 'source-map',
    entry: [
      './main.js'
    ],
    output: {
      path: ROOT + 'dist',
      filename: 'app.bundle.js'
    },
    devServer: {
      inline: true,
      contentBase: 'dist',
      historyApiFallback: true,
      port: 3000,
      host: '0.0.0.0',
      allowedHosts: [
        '.amazonaws.com',
        'localhost'
      ]
    },
    plugins: [],
    env_variables: env_variables,
  }
}


