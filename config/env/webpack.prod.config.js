let webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

let env_variables = {
  'process.env': {
    DEBUG: JSON.stringify(false),
    API_URL: JSON.stringify('https://pacific-beach-93517.herokuapp.com')
  }
};

module.exports = function (ROOT) {
  return {
    entry: {
      main: './main.js',
      vendor: require('../vendorList')()
    },
    output: {
      path: ROOT + 'dist',
      filename: '[name].[chunkhash].js'
    },
    plugins: [
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.HashedModuleIdsPlugin() // making sure that vendor doesn't change when only src is changed
    ],
    env_variables: env_variables,
    mode: 'production',
    optimization: {
      // https://webpack.js.org/configuration/optimization/#optimization-minimizer
      minimizer: [
        //https://webpack.js.org/plugins/uglifyjs-webpack-plugin/
        new UglifyJsPlugin({
          test: /\.js($|\?)/i,
          cache: true,
          parallel: true,
          sourceMap: true
        })
      ],
      // This is just a default from webpack website: https://webpack.js.org/plugins/split-chunks-plugin/#optimization-splitchunks
      splitChunks: {
        chunks: 'async',
        minSize: 30000,
        maxSize: 0,
        minChunks: 1,
        maxAsyncRequests: 5,
        maxInitialRequests: 3,
        automaticNameDelimiter: '~',
        name: true,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    }
  };
};

