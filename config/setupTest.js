import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { JSDOM } from 'jsdom'

Enzyme.configure({ adapter: new Adapter() })

//Set up for Enzyme mount method to work
const jsdom = new JSDOM('<!doctype html><html><body></body></html>')
const { window } = jsdom
global.window = window
global.document = window.document
global.navigator = {
    userAgent: 'node.js',
}

let mockStorage = {}
global.sessionStorage = {
    setItem: (key, val) => Object.assign(mockStorage, {[key]: val}),
    getItem: (key) => mockStorage[key],
    clear: () => mockStorage = {}
}
