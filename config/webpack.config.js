const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const ROOT = __dirname + '/../'

module.exports = function (env) {
    var config = require('./env/webpack.dev.config')(ROOT)
    if (env) {
        console.log(`BUILDING FOR: ${env.NODE_ENV}`)
        if (env.NODE_ENV === 'production') {
            config = require('./env/webpack.prod.config')(ROOT)
        }
    }
    return {
        context: ROOT + 'src',
        mode: config.mode,
        devtool: config.devtool,
        entry: config.entry,
        output: config.output,
        devServer: config.devServer,
        resolve: {
            modules: [path.resolve(ROOT, 'src'), 'node_modules'],
        },
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'eslint-loader',
                    options: {
                        fix: true
                    }
                },
                {
                    test: /\.js$/,
                    exclude: [/node_modules/],
                    loaders: ['babel-loader']
                },
                {
                    test: /\.(scss|css)$/,
                    use: [
                        'style-loader', // creates style nodes from JS strings
                        'css-loader', // translates CSS into CommonJS
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: './config/postcss.config.js'
                                }
                            }
                        },
                        'sass-loader' // compiles Sass to CSS
                    ]
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: [
                        'file-loader'
                    ]
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({  // Also generate a test.html
                filename: 'index.html',
                template: 'assets/index.html'
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery'
            }),
            new webpack.DefinePlugin(config.env_variables),
        ].concat(config.plugins)
    }
}


