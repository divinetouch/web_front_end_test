module.exports = function () {
  return [
    "axios",
    "bootstrap",
    "connected-react-router",
    "history",
    "react",
    "react-device",
    "react-dom",
    "react-helmet",
    "react-redux",
    "react-router-dom",
    "react-toastify",
    "reactstrap",
    "redux",
    "redux-logger",
    "redux-saga"
  ]
}