import axios from 'axios'

let instance = axios.create({
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    timeout: 5000,
    baseURL: process.env.API_URL
})

const
    PUT = 'PUT',
    POST = 'POST',
    PATCH = 'PATCH',
    GET = 'GET',
    DELETE = 'DELETE'

/**
 * A request method that return a plain JavaScript promise object 
 * 
 * @param {const} action : POST, GET, PATCH, DELETE, PUT
 * @param {string} url : the resource URI
 * @param {object} [data] : Data object that need to be sent to the API server
 * @returns {Promise} : return a Promise object
 */
let makeRequest = (action, url, data) => {
    return new Promise((resolve) => {
        return instance[action.toLowerCase()](url, data).then(response => {
            resolve(response.data)
        }).catch(error => {
            resolve({ error })
        })
    })
}

export const api = {

    /**
     * Authenticate user 
     * 
     * @param {Object} payload : {username: {string}, password: {string}}
     * @returns {Promise} : return a Promise object
     */
    login(payload) {
        return makeRequest(POST, '/user_login', payload)
    },

    logout(payload) {
        return makeRequest(DELETE, '/user_login', payload)
    },
}
