import 'regenerator-runtime/runtime'
import React from 'react'
import {render} from 'react-dom'
import App from './components/app/app'
import 'bootstrap/scss/bootstrap.scss'
import './assets/styles.scss'

render(<App />, document.getElementById('app'))