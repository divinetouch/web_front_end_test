import React from 'react'
import Authenticated from '../authenticated.js'
import { mount } from 'enzyme'
import LoginPage from '../../pages/loginPage/loginPage'
import { createStore } from 'redux'
import { appReducer, appConstants } from '../../app'

let store, wrapper, TestComponent

describe('<Authenticated/>', function() {

    beforeAll(() => {
        TestComponent = class extends React.Component {
            render() { return (<div id="test">Authenticated Component</div>) }
        }
        let Hoc = Authenticated(TestComponent)
        store = createStore(appReducer)
        wrapper = mount(<Hoc/>, {context: {store: store}})
    })

    it('should show Login when not authenticated', () => {
        expect(wrapper.find(LoginPage)).toHaveLength(1)
    })

    it('should show protected components after log in', () => {
        store.dispatch({type: appConstants.UPDATE_TOKEN, payload: {token: 'test'}})
        wrapper.update()
        expect(wrapper.find(TestComponent)).toHaveLength(1)
    })
})



