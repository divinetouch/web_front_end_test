import React, { Component } from 'react'
import LoginPage from '../pages/loginPage/loginPage'
import { connect } from 'react-redux'
import { appConstants, appActions } from '../app'

const mapStateToProps = (state) => ({
    token: state[appConstants.NAMESPACE].token
})

let Authenticated = (ComposedComponent) =>{
    class AuthenticatedComponent extends Component {

        componentWillMount() {
            let token = sessionStorage.getItem('token')
            token ? this.props.dispatch(appActions.updateTokenAction({token})) : null
        }

        componentWillReceiveProps(nextProps) {
            sessionStorage.setItem('token', nextProps.token)
        }

        render() {
            return (
                this.props.token ? <ComposedComponent {...this.props} /> : <LoginPage {...this.props} />
            )
        }
    }

    return connect(mapStateToProps)(AuthenticatedComponent)
}

export default Authenticated