import React from 'react'
import { Nav, NavItem } from 'reactstrap'
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { appConstants } from '../app'
import { logoutActions } from '../logout'
import './assets/navigationBar.scss'

const mapStateToProps = (state) => ({
    token: state[appConstants.NAMESPACE].token
})

export const NavigationBar = class extends React.Component {
    logout = (event) => {
        event.preventDefault()
        this.props.dispatch(logoutActions.logoutAction())
    }
    render() {
        return (
            <div className="nav-bar">
                <Nav pills>
                    <NavItem>
                        <NavLink className="nav-link" to="/home">Home</NavLink>
                    </NavItem>
                    {this.props.token ?
                        <React.Fragment>
                            <NavItem>
                                <NavLink className="nav-link" to="/profile">Profile</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink onClick={this.logout} className="nav-link" to="#">Log Out</NavLink>
                            </NavItem>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <NavItem>
                                <NavLink className="nav-link" to="/register">Register</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to="/sign_in">Log In</NavLink>
                            </NavItem>
                        </React.Fragment>
                    }
                </Nav>
                <hr />
            </div>
        )
    }
}

export default withRouter(connect(mapStateToProps)(NavigationBar))