import React from 'react'
import { mount } from 'enzyme'
import { NavigationBar } from '../navigationBar'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

describe('<Navigation />', () => {
    it('should not render profile if not log in', () => {
        const wrapper = mount(<Router history={history}><NavigationBar token="" /></Router>)
        expect(wrapper.html()).toContain('register')
    })

    it('should render profile after log in', () => {
        const wrapper = mount(<Router history={history}><NavigationBar token="abc" /></Router>)
        expect(wrapper.html()).toContain('profile')
    })

    it('should show logout link after log in', () => {
        const wrapper = mount(<Router history={history}><NavigationBar token="abc" /></Router>)
        expect(wrapper.html().toLowerCase()).toContain('log out')
    })
})