import { logout, LOADING_MESSAGE, UNHANDLED_ERROR, getToken } from '../index'
import { appConstants } from '../../app'
import { put, call, select } from 'redux-saga/effects'
import { api } from '../../../services/api'
import { push } from 'connected-react-router'

describe('<Logout />', () => {
    describe('SAGA', () => {
        it(`should call logout api and disptach ${appConstants.UPDATE_TOKEN}`, () => {
            const actions = { payload: { token: 'abc' } }
            const iterator = logout(actions)

            expect(iterator.next().value).toMatchObject(select(getToken))

            expect(iterator.next(actions.payload.token).value).toMatchObject(
                put({ type: appConstants.UPDATE_LOADING_STATUS, payload: { status: true, message: LOADING_MESSAGE } }))

            expect(iterator.next().value).toMatchObject(call(api.logout, actions))

            expect(iterator.next().value).toMatchObject(put({ type: appConstants.UPDATE_TOKEN, payload: { token: '' } }))

            expect(iterator.next().value).toMatchObject(put({ type: appConstants.UPDATE_LOADING_STATUS, payload: { status: false, message: '' } }))

            expect(iterator.next().value).toMatchObject(put(push('/sign_in')))
        })

        it(`should handle error and disptach ${appConstants.ERROR}`, () => {
            const actions = { payload: { token: 'abc' } }
            const iterator = logout(actions)

            iterator.next()

            expect(iterator.throw(UNHANDLED_ERROR).value).toMatchObject(put({ type: appConstants.ERROR, payload: { message: UNHANDLED_ERROR } }))

        })
    })
})