import { takeEvery, put, call, select } from 'redux-saga/effects'
import { appConstants } from '../app'
import { api } from '../../services/api'
import { push } from 'connected-react-router'

export const getToken = (state) => state[appConstants.NAMESPACE].token

const NAMESPACE = 'components/logout'
const logoutConstants = {
    LOGOUT: `${NAMESPACE}.LOGOUT`
}

export const LOADING_MESSAGE = 'Loging out..'
export const UNHANDLED_ERROR = `${logoutConstants.NAMESPACE}.UNHANDLED_ERROR`

const logoutActions = {
    logoutAction: (payload) => ({ type: logoutConstants.LOGOUT, payload })
}

export const logout = function* () {
    try {
        const token = yield select(getToken)
        yield put({ type: appConstants.UPDATE_LOADING_STATUS, payload: { status: true, message: LOADING_MESSAGE } })
        yield call(api.logout, { payload: { token: token } })
        yield put({ type: appConstants.UPDATE_TOKEN, payload: { token: '' } })
        yield put({ type: appConstants.UPDATE_LOADING_STATUS, payload: { status: false, message: '' } })
        yield put(push('/sign_in'))
    } catch (e) {
        yield put({ type: appConstants.ERROR, payload: { message: UNHANDLED_ERROR } })
    }
}

const logoutSaga = function* () {
    yield takeEvery(logoutConstants.LOGOUT, logout)
}

export {
    logoutConstants,
    logoutActions,
    logoutSaga,
}