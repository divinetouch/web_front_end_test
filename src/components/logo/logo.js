import React  from 'react'
import './assets/logo.scss'
import appConstants from '../app/appConstants'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    isDesktop: state[appConstants.NAMESPACE].isDesktop
})

export const Logo = (props) => {
    return (
        <div className="logo">
            <div className="logo-image-container">
                {props.isDesktop ? <div className="desktop-logo-image" role="logo"></div> :
                    <div className="mobile-logo-image" role="logo"></div>}
            </div>
        </div>
    )
}

export default connect(mapStateToProps)(Logo)
