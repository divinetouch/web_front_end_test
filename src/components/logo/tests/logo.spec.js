import React from 'react'
import {Logo} from '../logo'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

describe('<Logo/>', () => {

    let render = (isDesktop = false) => {
        return shallow(<Logo isDesktop={isDesktop} />)
    }

    it('should match snapshot', () => {
        const wrapper = render()
        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('render desktop version', () => {
        const wrapper = render(true)
        expect(wrapper.find('.desktop-logo-image')).toHaveLength(1)
    })

    it('render desktop version', () => {
        const wrapper = render(false)
        expect(wrapper.find('.mobile-logo-image')).toHaveLength(1)
    })
})