import React from 'react'
import ProfileForm from '../../profileForm/profileForm'

export default () => {
    return (<ProfileForm headerText='Enter your information below for exclusive offers, promotions and savings' submitButtonName='Register' />)
}