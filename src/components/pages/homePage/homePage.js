import React from 'react'

export default() => {
    return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems:'center', color: 'blue', margin: '2em'}}>
            <h1>Welcome!</h1>
        </div>
    )
}