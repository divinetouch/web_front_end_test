import RegistrationPage from './registrationPage/registrationPage'
import NotFoundPage from './notFoundPage/notFoundPage'
import LoginPage from './loginPage/loginPage'
import ProfilePage from './profilePage/profilePage'
import HomePage from './homePage/homePage'

export {
    RegistrationPage,
    NotFoundPage, 
    LoginPage,
    ProfilePage,
    HomePage,
}