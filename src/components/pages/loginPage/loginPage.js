import React, {Component} from 'react'
import LoginForm from '../../loginForm/loginForm'

const LoginPage = class extends Component {
    render() {
        return (
            <LoginForm
                formSubmit={this.formSubmit}
                successRoute = "/home"
                registerRoute= "/register"
            />
        )
    }
}

export default LoginPage