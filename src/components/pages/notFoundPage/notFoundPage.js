import React from 'react'

export default() => {
    return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems:'center', color: 'red', margin: '2em'}}>
            <h1>Page Not Found</h1>
        </div>
    )
}