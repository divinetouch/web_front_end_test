import React from 'react'
import ProfileForm from '../../profileForm/profileForm'

export default () => {
    return (<ProfileForm headerText='Profile Information' submitButtonName='Update' />)
}