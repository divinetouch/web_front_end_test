import React from 'react'
import { shallow } from 'enzyme'
import { Template } from '../template'
import toJson from 'enzyme-to-json'

describe('<Template/>', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Template 
            location={{pathname: '/'}} 
            loading={{status: false, message: ''}} />
        ) 

        expect(toJson(wrapper)).toMatchSnapshot()
    })
})