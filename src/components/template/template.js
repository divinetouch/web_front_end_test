import React, { Component } from 'react'
import Logo from '../logo/logo'
import Banner from '../banner/banner'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { appActions, appConstants } from '../app'
import { withRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import { Alert } from 'reactstrap'
import Loading from '../loading/loading'
import NavigationBar from '../navigationBar/navigationBar'

import 'react-toastify/dist/ReactToastify.css'

const mapStateToProps = (state) => ({
    pageName: state[appConstants.NAMESPACE].pageName,
    errorMessage: state[appConstants.NAMESPACE].error.message,
    loading: state[appConstants.NAMESPACE].loading,
})

export const Template = class extends Component {
    componentDidMount() {
        if(this.props.location.pathname !== '/') {
            this.props.dispatch(appActions.updatePageNameAction({pageName: this.props.location.pathname.replace('/', '')}))
        }
    }
    componentWillUpdate(nextProps) {
        const locationChanged = nextProps.location !== this.props.location
        if(locationChanged) {
            this.props.dispatch(appActions.updatePageNameAction({pageName: nextProps.location.pathname.replace('/', '')}))
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>{this.props.pageName}</title>
                </Helmet>
                <ToastContainer />
                <div className="container">
                    <Logo/>
                    <Banner/>
                    <NavigationBar />
                    <div style={{marginTop: '1em'}}>
                        {this.props.errorMessage && <Alert color="danger">
                            {this.props.errorMessage}
                        </Alert>}
                    </div>
                    {this.props.children}
                </div>
                {this.props.loading.status && <Loading loadingMessage={this.props.loading.message}/>}
            </React.Fragment>
        )
    }
}

export default withRouter(connect(mapStateToProps)(Template))