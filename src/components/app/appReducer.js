import { combineReducers } from 'redux'
import appConstants from './appConstants'

export const initialState = {
    isDesktop: false,
    loading: {
        status: false,
        message: ''
    },
    pageName: 'Sign In',
    token: '',
    error: {
        message: ''
    }
}

export const appReducer = (state = initialState, actions) => {
    switch(actions.type) {
        case appConstants.UPDATE_DEVICE_TYPE:
            return Object.assign({}, {...state, isDesktop: actions.payload.isDesktop})
        case appConstants.UPDATE_PAGE_NAME:
            return Object.assign({}, {...state, pageName: actions.payload.pageName})
        case appConstants.UPDATE_TOKEN:
            return Object.assign({}, {...state, token: actions.payload.token})
        case appConstants.ERROR:
            return Object.assign({}, {
                ...state, 
                error: Object.assign({}, {
                    ...state.error, 
                    message: actions.payload.message 
                })
            })
        case appConstants.UPDATE_LOADING_STATUS:
            return Object.assign({}, {...state, loading: actions.payload})
        default:
            return state
    }
}

const reducer = combineReducers({
    [appConstants.NAMESPACE]: appReducer,
})

export default reducer