import appConstants from './appConstants'

const updateDeviceTypeAction = (payload) => ({type: appConstants.UPDATE_DEVICE_TYPE, payload})
const updatePageNameAction = (payload) => ({type: appConstants.UPDATE_PAGE_NAME, payload})
const updateTokenAction = (payload) => ({type: appConstants.UPDATE_TOKEN, payload})

export default {
    updateDeviceTypeAction,
    updatePageNameAction,
    updateTokenAction,

}