import React from 'react'
import App from '../app'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
import { appReducer, initialState } from '../appReducer'
import constants from '../appConstants'

describe('<App/>', () => {
    const wrapper = shallow(<App/>)

    it('render all components', () => {
        expect(wrapper.find(Route)).toHaveLength(4)
        expect(wrapper.find(Switch)).toHaveLength(1)
    })

    it('should match snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })

    describe('APP_REDUCER', () => {
        it(`should update device type to desktop when dispatch action ${constants.UPDATE_DEVICE_TYPE}`, () => {
            const newState = appReducer(
                initialState,
                {
                    type: constants.UPDATE_DEVICE_TYPE,
                    payload: {isDesktop: true}
                },
            ) 

            expect(newState).toHaveProperty('isDesktop', true)
        })

        it(`should update page name when dispatch action ${constants.UPDATE_PAGE_NAME}`, () => {
            const pageName = 'test'
            const newState = appReducer(
                initialState,
                {
                    type: constants.UPDATE_PAGE_NAME,
                    payload: {pageName}
                },
            ) 

            expect(newState).toHaveProperty('pageName', pageName)
        })

        it(`should update error when dispatch action ${constants.ERROR}`, () => {
            const errorMessage = 'error'
            const newState = appReducer(
                initialState,
                {
                    type: constants.ERROR,
                    payload: {message: errorMessage}
                },
            ) 

            expect(newState).toHaveProperty('error', {message: errorMessage})
        })

        it(`should update loading status when dispatch action ${constants.UPDATE_LOADING_STATUS}`, () => {
            const newState = appReducer(
                initialState,
                {
                    type: constants.UPDATE_LOADING_STATUS,
                    payload: {status: true, message: 'loading'}
                },
            ) 

            expect(newState).toHaveProperty('loading', {status: true, message: 'loading'})
        })

    })

    // describe('APP_SAGA', () => {
    //     const actions = {payload: {isDesktop: true}}

    //     it(`should dispatch ${constants.UPDATE_DEVICE_TYPE}`, () => {
    //         const iterator = updateDeviceType(actions)

    //         expect(iterator.next().value).toMatchObject(put({type: constants.UPDATE_DEVICE_TYPE, payload: actions.payload}))
    //     })

    //     it('should handle throwable error', () => {
    //         const iterator = updateDeviceType(actions)
    //         const errorMessage = 'Unhandled error'
    //         iterator.next()
    //         expect(iterator.throw(new Error(errorMessage)).value).toMatchObject(put({type: constants.ERROR, payload: {message: errorMessage}}))
    //     })
    // })
})