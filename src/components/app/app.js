import React, { Component } from 'react'
import Template from '../template/template'
import * as Pages from '../pages'
import './assets/app.scss'
import Device from 'react-device'
import { Route, Switch } from 'react-router-dom'
import Authenticated from '../authenticated/authenticated'
import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import { Provider } from 'react-redux'
import reducer from './appReducer'
import appSaga from './appSaga'
import appActions from './appActions'
import { connectRouter, routerMiddleware, ConnectedRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import createSagaMiddleware from 'redux-saga'

const history = createBrowserHistory()

const DESKTOP_WIDTH = 768
const sagaMiddleware =  createSagaMiddleware()
const logger = createLogger({
    duration: true,
    timestamp: true,
    collapsed: true
})

const store = createStore(connectRouter(history)(reducer), 
    compose (
        applyMiddleware(
            routerMiddleware(history),
            logger, 
            sagaMiddleware)
    )
)

sagaMiddleware.run(appSaga)

const App = class extends Component {

    _onChange = (deviceInfo) => {
        const isDesktop = deviceInfo.screen.width >= DESKTOP_WIDTH
        store.dispatch(appActions.updateDeviceTypeAction({isDesktop}))
    }

    render() {
        return (
            <Provider store={store}>
                <div>
                    <Device onChange={this._onChange}/>
                    <ConnectedRouter history={history}>
                        <Template>
                            <Switch>
                                <Route exact path="/register" component={Pages.RegistrationPage}/>
                                <Route exact path="/sign_in" component={Pages.LoginPage}/>
                                <Route exact path="/home" component={Pages.HomePage}/>
                                <Route path="/" component={Authenticated(Main)}/>
                            </Switch>
                        </Template>
                    </ConnectedRouter>
                </div>
            </Provider>
        )
    }
}

export default App

const Main = () => (
    <Switch>
        <Route exact path="/profile" component={Pages.ProfilePage}/>
        <Route exact path="/" component={Pages.HomePage}/>
        <Route component={Pages.NotFoundPage}/>
    </Switch>
)
