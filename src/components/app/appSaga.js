import { fork } from 'redux-saga/effects'
import { loginFormSaga } from '../loginForm'
import { logoutSaga } from '../logout'

const saga = function* rootSaga() {
    yield [
        fork(loginFormSaga),
        fork(logoutSaga),
    ]
}

export default saga