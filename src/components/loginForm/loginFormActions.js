import loginFormConstants from './loginFormConstants'

const loginAction = (payload) => ({type:loginFormConstants.LOGIN, payload})
const showErrorAction = (payload) => ({type: loginFormConstants.SHOW_ERROR, payload})
const clearErrorAction = (payload) => ({type: loginFormConstants.CLEAR_ERROR, payload})
const nextRouteAction = (payload) => ({type: loginFormConstants.NEXT_ROUTE, payload})

export default {
    loginAction,
    showErrorAction,
    clearErrorAction,
    nextRouteAction,
}