import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap'
import './assets/login_form.scss'
import { connect } from 'react-redux'
import loginFormActions from './loginFormActions'

const ERROR_MESSAGE = 'Username or Password cannot be empty'

export const LoginForm = class extends Component {

    usernameRef = React.createRef()
    passwordRef = React.createRef()

    state = {
        username: '',
        password: ''
    }

    updateState = (event) => {
        const target = event.target
        const name = target.name
        const value = target.value
        this.setState({[name]: value})
    }

    formSubmit = (event) => {
        event.preventDefault()
        this.props.dispatch(loginFormActions.clearErrorAction())
        if(!this.state.username) {
            this.props.dispatch(loginFormActions.showErrorAction({message: ERROR_MESSAGE}))
            this.usernameRef.focus()
        } else if (!this.state.password) {
            this.props.dispatch(loginFormActions.showErrorAction({message: ERROR_MESSAGE}))
            this.passwordRef.focus()
        } else {
            this.props.dispatch(loginFormActions.loginAction({...this.state, route: this.props.successRoute }))
        }
    }

    register = () => {
        this.props.dispatch(loginFormActions.nextRouteAction({route: this.props.registerRoute}))
    }

    render() {
        return (
            <div className="login-form">
                <section id="description-section">
                    <div id="text">
                        Sign In
                    </div>
                </section>
                <section id="form-section">
                    <Form role="form">
                        <div id="input-section">
                            <FormGroup>
                                <Label name="username" for="username" className="required">User Name</Label>
                                <Input
                                    innerRef={input => this.usernameRef = input}
                                    type="text" 
                                    name="username" 
                                    id="username" 
                                    placeholder="" 
                                    value={this.state.username} 
                                    onChange={this.updateState} />
                            </FormGroup>
                            <FormGroup>
                                <Label name="password" for="password" className="required">Password</Label>
                                <Input 
                                    innerRef={input => this.passwordRef = input}
                                    type="text" 
                                    name="password" 
                                    id="password" 
                                    placeholder="" 
                                    value={this.state.password} 
                                    onChange={this.updateState} />
                            </FormGroup>
                        </div>
                        <div id="button-section">
                            <div className="form-button">
                                <Button 
                                    onClick={this.formSubmit} 
                                    className="btn-block">Log In
                                </Button>
                            </div>
                            <div className="form-button">
                                <Button 
                                    onClick={this.register} 
                                    className="btn-block">Register 
                                </Button>
                            </div>
                        </div>
                    </Form>
                </section>
            </div>
        )
    }
}

export default connect()(LoginForm)