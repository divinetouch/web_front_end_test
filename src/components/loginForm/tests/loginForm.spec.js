import React from 'react'
import { shallow } from 'enzyme'
import { LoginForm } from '../loginForm'
import toJson from 'enzyme-to-json'
import { login, UNHANDLED_ERROR, generateErrorMessage, LOADING_MESSAGE, nextRoute } from '../loginFormSaga'
import { api } from '../../../services/api'
import { appConstants } from '../../app'
import { call, put } from 'redux-saga/effects'

describe('<Form/>', () => {
    const wrapper = shallow(<LoginForm />) 

    it('render description section and form section', () => {
        expect(wrapper.find('#description-section')).toHaveLength(1)
        expect(wrapper.find('#form-section')).toHaveLength(1)
    })

    it('should match snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })

    describe('SAGA', () => {
        const actions = {payload: {username: 'test', password: 'test', route: '/test'}}
        const response = {token: 'abc'}
        const responseError = {error: {message: 'error'}}

        it(`should dispach ${appConstants.UPDATE_TOKEN} action when token is given`, () => {
            let iterator = login(actions)

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: true, message: LOADING_MESSAGE}}))

            expect(iterator.next().value).toMatchObject(call(api.login, actions.payload))

            expect(iterator.next(response).value).toMatchObject(put({type: appConstants.UPDATE_TOKEN, payload: {token: response.token}}))

            expect(iterator.next(response).value).toMatchObject(call(nextRoute, actions))

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: false, message: ''}}))
        })

        it(`should dispach ${appConstants.ERROR} action when error in response`, () => {
            let iterator = login(actions)

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: true}}))

            expect(iterator.next().value).toMatchObject(call(api.login, actions.payload))

            expect(iterator.next(responseError).value).toMatchObject(
                put({type: appConstants.ERROR, payload: {message: generateErrorMessage(responseError.error.message)}}))

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: false}}))
        })

        it(`should dispach ${appConstants.ERROR} action when accounter unhandled error`, () => {
            let iterator = login(actions)

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: true}}))

            expect(iterator.next().value).toMatchObject(call(api.login, actions.payload))

            expect(iterator.throw(new Error(UNHANDLED_ERROR)).value).toMatchObject(
                put({type: appConstants.ERROR, payload: {message: UNHANDLED_ERROR}}))

            expect(iterator.next().value).toMatchObject(put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: false}}))
        })
    })
})