import { takeEvery, put, call } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import loginFormConstants from './loginFormConstants'
import { api } from '../../services/api'
import { appConstants } from '../app'
import { push } from 'connected-react-router'

export const UNHANDLED_ERROR = `${loginFormConstants.NAMESPACE}.UNHANDLED_ERROR`
export const LOADING_MESSAGE = 'Validating Credentials'

export const generateErrorMessage = (message) => `${loginFormConstants.NAMESPACE}: ${message ? message : 'no message from server'}`

export const login = function* (actions) {
    try {
        yield put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: true, message: LOADING_MESSAGE}})
        const response = yield call(api.login, actions.payload)
        if(response.token) {
            yield put({type: appConstants.UPDATE_TOKEN, payload: {token: response.token}})
            yield call(nextRoute, actions)
        } else {
            yield put({type: appConstants.ERROR, payload: {message: generateErrorMessage(response.error.message)}})
        }
    } catch (e) {
        yield put({type: appConstants.ERROR, payload: {message: UNHANDLED_ERROR}})
    } finally {
        yield put({type: appConstants.UPDATE_LOADING_STATUS, payload: {status: false, message: ''}})
    }
}

export function* showError(actions) {
    yield toast.error(actions.payload.message, {position: toast.POSITION.TOP_CENTER})
}

export function* clearError() {
    yield put({type: appConstants.ERROR, payload: {message: ''}})
}

//TODO: create a routing component instead
export function* nextRoute(actions) {
    yield put(push(actions.payload.route))
}

export default function* loginFormSaga() {
    yield takeEvery(loginFormConstants.LOGIN, login)
    yield takeEvery(loginFormConstants.SHOW_ERROR, showError)
    yield takeEvery(loginFormConstants.CLEAR_ERROR, clearError)
    yield takeEvery(loginFormConstants.NEXT_ROUTE, nextRoute)
}