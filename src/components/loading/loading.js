import React from 'react'
import './assets/loading.scss'

export default (props) => {
    return (
        <div className="loading">
            <div className='lds-dual-ring-huge'>
                <div className='lds-dual-ring-bigger'>
                    <div className='lds-dual-ring-big'>
                        <div className='lds-dual-ring-small'>
                        </div>
                    </div>
                </div>
            </div>
            <div className="loading-message">
                {props.loadingMessage}
            </div>
        </div>
    )
}
