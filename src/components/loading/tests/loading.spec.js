import React from 'react'
import { shallow } from 'enzyme'
import Loading from '../loading'
import toJson from 'enzyme-to-json'

describe('<Loading/>', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Loading />)
        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('should render with message prop', () => {
        const message = 'loading'
        const wrapper = shallow(<Loading loadingMessage='loading'/>)
        expect(wrapper.html()).toContain(message)
    })
})