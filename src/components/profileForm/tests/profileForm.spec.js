import React from 'react'
import { shallow } from 'enzyme'
import {ProfileForm} from '../profileForm'
import toJson from 'enzyme-to-json'

describe('<Form/>', () => {
    let wrapper = shallow(<ProfileForm />) 

    it('should match snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('render description section and form section', () => {
        expect(wrapper.find('#description-section')).toHaveLength(1)
        expect(wrapper.find('#form-section')).toHaveLength(1)
    })

    it('render error section when there is at least one error', () => {
        expect(wrapper.find('#error-section')).toHaveLength(0)
        wrapper.setState({errorList: ['Invalid Email'], hasError: true})
        expect(wrapper.find('#error-section')).toHaveLength(1)
        expect(wrapper.find('li')).toHaveLength(1)
    })

    it('render with submit button name as Register', () => {
        expect(wrapper.find('Button').html()).toContain('Register')
    })

    it('render with submit button name as Update', () => {
        wrapper = shallow(<ProfileForm submitButtonName="Update"/>)
        expect(wrapper.find('Button').html()).toContain('Update')
    })

})
