import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input, InputGroup } from 'reactstrap'
import CustomPopOver from '../popOver/popOver'
import CustomDropDown from '../dropDown/dropDown'
import './assets/registration_form.scss'
import { connect } from 'react-redux'
import appConstants from '../app/appConstants'

const mapStateToProps = (state) => ({
    isDesktop: state[appConstants.NAMESPACE].isDesktop
})

export const ProfileForm = class extends Component {

    formRef = React.createRef()

    state = {
        monthOptions: [
            'January',
            'February', 
            'March', 
            'April', 
            'May', 
            'June', 
            'July', 
            'August', 
            'September', 
            'October', 
            'November', 
            'December'
        ],
        dayOptions: [],
        yearOptions: [],
        target: 'birthday-question',
        inputNames: {
            firstname: {
                hasError: false,
                required: true,
                value: '',
                error: 'Invalid firstname',
                otherTargetName: ['required-field']
            },
            lastname: {
                hasError: false,
                required: false,
                value: '',
                error: 'Invalid lastname',
                otherTargetName: ['required-field']
            },
            emailaddress: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Email Address',
                otherTargetName: ['required-field']
            },
            password: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Password',
                otherTargetName: ['required-field']
            },
            passwordconfirm: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Password',
                match: 'password',
                otherTargetName: ['required-field']
            },
            phonenumber: {
                hasError: false,
                value: '',
                error: 'Invalid Phone Number',
                regex: /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/i,
                otherTargetName: ['required-field']
            },
            month: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Month',
                otherTargetName: ['birthdate', 'required-field']
            },
            day: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Day',
                otherTargetName: ['birthdate', 'required-field']
            },
            year: {
                hasError: false,
                required: true,
                value: '',
                error: 'Invalid Year',
                otherTargetName: ['birthdate', 'required-field']
            },
            country: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Country',
                otherTargetName: ['required-field']
            },
            zipcode: {
                required: true,
                hasError: false,
                value: '',
                error: 'Invalid Zip Code',
                regex: /^\d{5}(?:[-\s]\d{4})?$/i,
                otherTargetName: ['required-field']
            },
            terms: {
                required: true,
                hasError: false,
                value: false,
                error: 'You must accept the terms & conditions and privacy policy',
                otherTargetName: ['required-field']
            },
            offer: {
                value: false,
                hasError: false
            }
        },
        hasError: false,
        errorList: [],
        otherErrorTargets: new Set()
    }

    componentWillMount() {
        let dayAr = []
        let yearAr = []
        for(let i = 1; i <= 31; i++) {
            dayAr.push(i)
        }
        for(let i = 2018; i >= 2018-110; i--) {
            yearAr.push(i)
        }
        this.setState({dayOptions: dayAr, yearOptions: yearAr})
    }

    renderOptions(options) {
        return options.map((d, index) => {
            return <option key={index} value={d} >{d}</option>
        })
    }

    updateState = (event) => {
        const target = event.target
        const name = target.name
        const value = target.type === 'checkbox' ? target.checked : target.value
        let inputNames = Object.assign({}, this.state.inputNames)
        inputNames[name].value = value
        this.setState(inputNames)
    }

    clearError() {
        const names = Object.assign({}, this.state.inputNames)
        Object.keys(names).forEach(key => {
            names[key].hasError = false
        })
        this.setState({hasError: false, otherErrorTargets: new Set(), errorList: [], inputNames: names})
    }

    validateForm() {
        const names = Object.assign({}, this.state.inputNames)
        const errorList = new Set()
        const otherErrorTargets = []
        Object.keys(names).forEach(key => {
            if(!names[key].value && names[key].required) {
                names[key].hasError = true
                errorList.add(names[key].error)
                if(names[key].otherTargetName.length) {
                    otherErrorTargets.push.apply(otherErrorTargets, names[key].otherTargetName)
                }
            } else if (names[key].regex) {
                names[key].hasError = !names[key].regex.test(names[key].value)
                names[key].hasError ? errorList.add(names[key].error) : null
            } else if (names[key].match && names[key].value !== names[names[key].match].value) {
                names[key].hasError = true
                errorList.add(names[key].error)
            }
        })
        
        this.setState({
            hasError: errorList.size > 0, 
            otherErrorTargets: new Set(otherErrorTargets), 
            errorList: Array.from(errorList), 
            inputNames: names
        })

        return errorList.size === 0
    }

    formSubmit = () => {
        this.clearError()
        if(this.validateForm()) {
            alert('Form is valid!')
        }
    }

    popOverToggle = () => {
        this.setState({popoverOpen: !this.state.popoverOpen})
    }

    dropDownToggle = () => {
        this.setState({dropdownOpen: !this.state.dropdownOpen})
    }

    renderErrorList = () => {
        return (
            <ul>
                {this.state.errorList.map(error => {
                    return (<li key={error}>{error}</li>)
                })}
            </ul>
        )
    }

    renderClassName(key) {
        return (
            `${this.state.inputNames[key].required? 'required' : ''}` + `${this.state.inputNames[key].hasError ? ' error' : ''}`
        )
    }

    render() {
        return (
            <div className="register-form">
                <div className="form-container">
                    <section id="description-section">
                        <div id="text">
                            Enter your information below for exclusive offers, promotions and savings
                        </div>
                    </section>
                    <section id="form-section">
                        <Form id="reg-form" ref={this.formRef} role="form">
                            <div id="input-section">
                                <FormGroup>
                                    <Label id="required-field" name="required-field" className={this.state.otherErrorTargets.has('required-field') ? 'error': ''}>* Fields required</Label>
                                </FormGroup>
                                <FormGroup>
                                    <Label name="firstname" for="firstname" className={this.renderClassName('firstname')}>First Name</Label>
                                    <Input 
                                        type="text" 
                                        name="firstname" 
                                        id="firstname" 
                                        placeholder="" 
                                        value={this.state.inputNames.firstname.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="lastname" name="lastname">Last Name</Label>
                                    <Input 
                                        type="text" 
                                        name="lastname" 
                                        id="lastname" 
                                        placeholder="" 
                                        value={this.state.inputNames.lastname.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="emailaddress" name="emailaddress" className={this.renderClassName('emailaddress')}>Email Address</Label>
                                    <Input 
                                        type="email" 
                                        name="emailaddress" 
                                        id="emailaddress" 
                                        placeholder="" 
                                        value={this.state.inputNames.emailaddress.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="password" className={this.renderClassName('password')} name="password">Choose Password</Label>
                                    <Input 
                                        type="password" 
                                        name="password" 
                                        id="password" 
                                        placeholder="" 
                                        value={this.state.inputNames.password.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="passwordconfirm"  className={this.renderClassName('passwordconfirm')} name="passwordconfirm">Confirm Password</Label>
                                    <Input 
                                        type="password" 
                                        name="passwordconfirm" 
                                        id="passwordconfirm" 
                                        placeholder="" 
                                        value={this.state.inputNames.passwordconfirm.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <div id="birthdate-header-section">
                                        <Label for="birthdate" className={`required ${this.state.otherErrorTargets.has('birthdate') ? 'error': ''}`} name="birthdate" id="birthdate">Birthdate</Label>
                                        <div>
                                            <div id={this.state.target} onClick={this.popOverToggle} className="question-mark-icon">
                                                {this.props.isDesktop ? <div id="desktop-birthday-question-mark"></div> :
                                                    <div id="mobile-birthday-question-mark"></div>}
                                            </div>
                                            <CustomPopOver 
                                                target={this.state.target} 
                                                toggle={this.popOverToggle} 
                                                popoverOpen={this.state.popoverOpen}/>
                                        </div>
                                    </div>
                                    <InputGroup id="birthdate-input-section">
                                        <InputGroup className="birthdate">
                                            <CustomDropDown 
                                                name="month"
                                                hasError={this.state.inputNames.month.hasError}
                                                required={true}
                                                handleSelected={this.updateState} 
                                                items={this.state.monthOptions} 
                                                selected={this.state.inputNames.month.value} 
                                                isDesktop={this.props.isDesktop} 
                                            />
                                        </InputGroup>
                                        <InputGroup className="birthdate">
                                            <CustomDropDown 
                                                name="day"
                                                hasError={this.state.inputNames.day.hasError}
                                                required={true}
                                                handleSelected={this.updateState} 
                                                items={this.state.dayOptions} 
                                                selected={this.state.inputNames.day.value} 
                                                isDesktop={this.props.isDesktop} 
                                            />
                                        </InputGroup>
                                        <InputGroup className="birthdate">
                                            <CustomDropDown
                                                name="year"
                                                hasError={this.state.inputNames.year.hasError}
                                                required={true}
                                                handleSelected={this.updateState} 
                                                items={this.state.yearOptions} 
                                                selected={this.state.inputNames.year.value} 
                                                isDesktop={this.props.isDesktop} 
                                            />
                                        </InputGroup>
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <div id="phone-header-section">
                                        <Label for="phonenumber" name="phonenumber" className={this.state.inputNames.phonenumber.hasError ? 'error' : ''}>Phone Number</Label>
                                        <Label id="phone-description">
                                            Lorem ipsum dolor.
                                        </Label>
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="phonenumber" 
                                        id="phonenumber" 
                                        placeholder="(xxx) xxx-xxxx" 
                                        className={`${this.state.inputNames.phonenumber.hasError ? 'error' : ''}`} 
                                        value={this.state.inputNames.phonenumber.value} 
                                        onChange={this.updateState} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="country" className={this.renderClassName('country')} name="country">Country</Label>
                                    <InputGroup id="country-input-group">
                                        <Label id="icon-usa">
                                            <Input type="radio" name="country" value="Usa" checked={this.state.inputNames.country.value === 'Usa'} onChange={this.updateState} />{' '}
                                            {this.props.isDesktop ? <div id="country-desktop-usa" role="image"/> :
                                                <div id="country-mobile-usa" role="image" alt="Usa"/> }
                                        </Label>
                                        <Label id="icon-canada">
                                            <Input className="country-radio" type="radio" name="country" value="Canada" checked={this.state.inputNames.country.value === 'Canada'} onChange={this.updateState} />{' '}
                                            {this.props.isDesktop ? <div id="country-desktop-canada" role="image" alt="canada"/> :
                                                <div id="country-mobile-canada" role="image" alt="Canada"/> }
                                        </Label>
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="zipcode" className={this.renderClassName('zipcode')} name="zipcode">Zip/Postal Code</Label>
                                    <Input 
                                        className={`${this.state.inputNames.zipcode.hasError ? 'error' : ''}`} 
                                        type="text"
                                        name="zipcode" 
                                        id="zipcode" 
                                        placeholder=""  
                                        className={`${this.state.inputNames.zipcode.hasError ? 'error' : ''}`}
                                        onChange={this.updateState} />
                                </FormGroup>
                            </div>
                            <section id="agreement-section">
                                <FormGroup check>
                                    <Label check className={this.renderClassName('terms')} name="terms">
                                        <Input 
                                            className={`${this.state.inputNames.zipcode.hasError ? 'error' : ''}`} 
                                            type="checkbox" 
                                            name="terms" 
                                            id="terms" 
                                            checked={this.state.inputNames.terms.value} 
                                            onChange={this.updateState}/> {'  '}
                                        <span>Yes, I accept the <span className="underline">Terms & condition</span> and <span className="underline">Privacy Policy</span></span>
                                    </Label>
                                </FormGroup>
                                <FormGroup check style={{marginTop: '12px'}}>
                                    <Label check>
                                        <Input 
                                            type="checkbox" 
                                            name="offer" 
                                            id="offer" 
                                            checked={this.state.inputNames.offer.value} 
                                            onChange={this.updateState}/> {'  '}
                                        <span>Yes, I'd like to receive news and special offers</span>
                                    </Label>
                                </FormGroup>
                            </section>
                            <div id="submit-button">
                                <Button 
                                    onClick={this.formSubmit} 
                                    className="btn-block">{this.props.submitButtonName || 'Register'}
                                </Button>
                            </div>
                        </Form>
                    </section>
                    {this.state.hasError && <section id="error-section">
                        <p>The following errors have occurred:</p>
                        {this.renderErrorList()}
                    </section>}
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps)(ProfileForm)
