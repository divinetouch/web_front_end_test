import React from 'react'
import CustomPopOver from '../popOver'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

describe('<PopOver/>', () => {

    const toggle = jest.fn()
    const wrapper = shallow(<div><div id="my-id">question?</div><CustomPopOver target="my-id" popoverOpen={true} toggle={toggle}/></div>)

    it('should match snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })
})