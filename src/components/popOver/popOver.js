import React from 'react'
import { Popover, PopoverBody } from 'reactstrap'
import './assets/popOver.scss'

export default(props) => {
    return (
        <Popover placement="left" isOpen={props.popoverOpen} target={props.target} toggle={props.toggle}>
            <PopoverBody className="pop-over-body">We'll use this to send you birthday bonus points</PopoverBody>
        </Popover>
    )
}