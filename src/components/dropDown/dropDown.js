import React, { Component } from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import './assets/dropDown.scss'

export default class extends Component {
    state = {
        dropdownOpen: false
    }

    _dropDownToggle = () => {
        this.setState({dropdownOpen: !this.state.dropdownOpen})
    }

    _renderDropDown = () => {
        return this.props.items.map(item => {
            return (<DropdownItem key={item} value={item} onClick={this.props.handleSelected} name={this.props.name}>{item}</DropdownItem>)
        })
    }

    _renderDesktop() {
        return this.props.hasError ? <div id="desktop-carrot-error-icon"></div> : <div id="desktop-carrot-icon"></div>
    }

    _renderMobile() {
        return this.props.hasError ? <div id="mobile-carrot-error-icon"></div> : <div id="mobile-carrot-icon"></div>
    }

    render () {
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this._dropDownToggle}>
                <DropdownToggle className="birthdate-dropdown">
                    <div className="dropdown-input">
                        <div 
                            className={`capitalize ${this.props.required ? 'required': ''}`} 
                            name={this.props.name}
                            value={this.props.selected}>
                            {this.props.selected || this.props.name}
                        </div>
                        <div className="dropdown-icon">
                            {this.props.isDesktop ? this._renderDesktop() : this._renderMobile() }
                        </div>
                    </div>
                </DropdownToggle>
                <DropdownMenu>
                    {this._renderDropDown()}
                </DropdownMenu>
            </Dropdown>
        )
    }
}