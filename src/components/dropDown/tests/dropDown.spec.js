import React from 'react'
import CustomDropDown from '../dropDown'
import { shallow, mount } from 'enzyme'
import toJson from 'enzyme-to-json'

describe('<CustomDropDown/>', () => {
    const items = ['item1', 'item2', 'item3']
    let selected = items[0]
    const name = 'month'
    const handleSelected = jest.fn()
    const wrapper = shallow(<CustomDropDown hasError={true} items={items} name={name} selected={selected} handleSelected={handleSelected} />)

    it('should match snapshot', () => {
        expect(toJson(wrapper, {mode: 'shallow'})).toMatchSnapshot()
    })

    it('render dropdown list', () => {
        expect(wrapper.html()).toContain('item1')
        expect(wrapper.html()).toContain('item2')
    })

    it('render with given prop name', () => {
        const selector = `.capitalize[name="${name}"]`
        expect(wrapper.find(selector)).toHaveLength(1)
    })

    it('display picked item from the list', () => {
        const selector = `.capitalize[value="${selected}"]`
        expect(wrapper.find(selector)).toHaveLength(1)
    })

    it('call function to update selected item', () => {
        const mountWrapper = mount(<CustomDropDown items={items} name={name} selected={selected} handleSelected={handleSelected} />)
        mountWrapper.find(`.dropdown-item[value="${items[0]}"]`).simulate('click')
        expect(handleSelected.mock.calls.length).toBe(1)
    })

    it('to show red error icon', () => {
        const mountWrapper = mount(<CustomDropDown
            items={items}
            name={name}
            selected={selected}
            hasError={true}
            isDesktop={true}
            handleSelected={handleSelected} />)

        expect(mountWrapper.find(`#desktop-carrot-error-icon`)).toHaveLength(1)
    })
})
