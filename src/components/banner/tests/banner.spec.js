import React from 'react'
import Banner, {TEXT} from '../banner'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

describe('<Banner/>', () => {
    const wrapper = shallow(<Banner/>)

    it('render component properly', () => {
        expect(wrapper.find('.banner')).toHaveLength(1)
        expect(wrapper.text()).toContain(TEXT)
    })

    it('should match snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })
})