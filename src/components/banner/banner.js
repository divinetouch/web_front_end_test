import React from 'react'
import './assets/banner.scss'

export const TEXT = 'company message'

export default() => {
    return (
        <div className="banner" role="banner">
            <div id="text" role="text">{TEXT}</div>
        </div>
    )
}
