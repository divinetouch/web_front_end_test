# Introduction

The purpose of this React application is to demostrate how to integrate Redux, Redux-Saga, and React Router together. 

On top of that this application is also structured differently from a conventional react with redux application. This is an attempt to follow the [re-ducks](https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be) approach.

**NOTE:** With an application this small it is `unessary` to use Redux-Saga or even Redux.

# How to run the project:

1. Clone the project
2. Go to the application folder
3. Run command `npm run initial-setup`
4. Run command `npm start`. **NOTE** this is also going to run a test server.
5. Open the browser and go to `0.0.0.0:3000`, the test username is `test` and passowrd is `test`.

# How to run test:

1. Go to the application folder
2. Run command `npm test`

# How to build the project:

1. Clone the project
2. Go to the application folder
3. Run command `npm install`
4. Run command `npm run build:dev`

**Note**: All generated artifacts are in the dist folder.