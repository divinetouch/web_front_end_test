const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(cors())

// parse application/json
app.use(bodyParser.json())

app.post('/register', (req, res) => {
    setTimeout(() => {
        res.status(201).json({
            token: 'abc'
        })
    }, 1000)
})

app.put('/profile', (req, res) => {
    setTimeout(() => {
        res.status(204).json({
            message: 'success'
        })
    }, 1000)
})

app.post('/user_login', (req, res) => {
    if (req.body.username === 'test' && req.body.password === 'test') {
        setTimeout(() => {
            res.status(200).json({
                'token': 'abc'
            })
        }, 1000)
    } else {
        setTimeout(() => {
            res.status(401).json({
                'message': 'Unauthorized'
            })
        }, 1000)
    }
})

app.delete('/user_login', (req, res) => {
    setTimeout(() => {
        res.status(202).json({
            'message': 'success'
        })
    }, 1000)
})

const port = 8002
app.listen(port, () => {
    console.log('Running server on port ' + port)
})